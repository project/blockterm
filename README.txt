
== Description ==
BlockTerm lets you specify taxonomy terms to show this block on. 
 When you visit a page where the node is tagged with one of these terms, the block will show, otherwise it won't.
 
 
== Installation ==

1. Enable the module

2. add the following to [yourtheme]_block($block) in template.php:

<code>
if (module_exists('blockterm')) {
  if (!blockterm_can_show($block) ) {
    return;
  }
}

return phptemplate_block($block);

</code>
== Usage ==

1. Edit any Block, open up the fieldset for Block Taxonomy, select some terms

2. <strong> add *node/* to the list of allowed URLs (else this module will do nothing)

3. Save it and try it out!
